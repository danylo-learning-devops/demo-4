# Demo-4 project
Part of learning project for SS academy.<br />
This template create Jira instance (using Jira evaluation license). Fro VM creation use Terraform plan.
## Task src
```
Вимоги: 
- На момент демо всі проекти були Google Cloud Platform (GCP)
- ОС: Centos 7
- Описати проект в GitLab README
- Проекти зберігати в control versions (на Gitlab Саши)
- Кожна команда повинна підготувати презентацію про продукт (на англ)
- Доповідь повинна бути на англ
- Доповідь до 15хв з людини
- У кожного повинна бути презинтація, на титульні сторінці презинтації Імя та Фамілія
- Розподілення задач в команді обовязок самої команди :)

Задачі:

Комадна 1 (Саша, Віктор, Данило):
ТЗ:
Налаштувати наступну систему: 
- Zabbix моніторинг система
- jira таск менеджер (в ній створити команду Support)
- AWX менеджмент серверів
- 1 веб-сервер для тесту (має моніторить zabbix)
- якщо спрацьовує тригер на zabbix то AWX повинен створювати тікет в jira на коменду “Support”
- Всі інстанси в GCP повинні розгортатися за допопогою Terraform
- Розгортання повинно бути на Ansible
- паролі повинні зберігатися в hashicorp vault

SRC: https://docs.google.com/document/d/1yh3w1TrRtlepN-yLQWu3PmI7YS2j94q4X7Ng_v3_8A0/edit
```